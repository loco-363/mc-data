/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData
 * UpStream data
 */
#ifndef MCDATA_UP_H_
#define MCDATA_UP_H_

#include "mc-data/data/compressor_1_aux.h"
#include "mc-data/data/compressor_2.h"
#include "mc-data/data/control_mode.h"
#include "mc-data/data/coupling.h"
#include "mc-data/data/driving_mode.h"
#include "mc-data/data/fallback_control.h"
#include "mc-data/data/fans.h"
#include "mc-data/data/light.h"
#include "mc-data/data/mcc_braking.h"
#include "mc-data/data/mcc_direction.h"
#include "mc-data/data/mcc_driving.h"
#include "mc-data/data/on_off.h"
#include "mc-data/data/pantographs.h"
#include "mc-data/data/pos_light.h"
#include "mc-data/data/power_system_switch.h"
#include "mc-data/data/push_button.h"
#include "mc-data/data/reflector.h"
#include "mc-data/data/speed_keys.h"
#include "mc-data/data/tps_frequency.h"
#include "mc-data/data/traction_limit.h"
#include "mc-data/data/train_heating.h"
#include "mc-data/data/train_type.h"


namespace MCData {

struct Up {
	/** Panels */
	struct {
		/** P */
		struct {
			/** P-H */
			struct {
				/** P-H-S */
				struct {
				} s;
				
				/** P-H-P */
				struct {
				} p;
			} h;
			
			/** P-D */
			struct {
				/** P-D-L */
				struct {
					/**
					 * P-D-L-H1 => Main Switch Off
					 * P-D-L-H1 => Vypínání hlavního vypínače
					 * Circuit diagram part ... S-123 / S-124
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} h1;
					
					/**
					 * P-D-L-H2 => Power System Switching & Main Switch
					 * P-D-L-H2 => Přepínání systémů + hlavní vypínač
					 * Circuit diagram part ... S-125 / S-126
					 */
					struct {
						Data::PowerSystemSwitch position = Data::PowerSystemSwitch::Off;
					} h2;
					
					/**
					 * P-D-L-H3 => Fall-back Control
					 * P-D-L-H3 => Nouzová jízda
					 * Circuit diagram part ... S-195 / S-196
					 */
					struct {
						Data::FallbackControl position = Data::FallbackControl::Disabled;
					} h3;
					
					/**
					 * P-D-L-H4 => Train Heating
					 * P-D-L-H4 => Topení vlaku
					 * Circuit diagram part ... S-119 / S-120
					 */
					struct {
						Data::TrainHeating position = Data::TrainHeating::Off;
					} h4;
					
					/**
					 * P-D-L-H5 => Control
					 * P-D-L-H5 => Řízení
					 * Circuit diagram part ... S-101 / S-102
					 */
					struct {
						Data::OnOff enabled = Data::OnOff::Off;
					} h5;
					
					/**
					 * P-D-L-S1 => Pantographs
					 * P-D-L-S1 => Sběrače
					 * Circuit diagram part ... S-121 / S-122
					 */
					struct {
						Data::Pantographs position = Data::Pantographs::None;
					} s1;
					
					/**
					 * P-D-L-S2 => Compressor 1, Auxiliary compressor
					 * P-D-L-S2 => Kompresor 1, Pomocný kompresor
					 * Circuit diagram part ... S-115 / S-116
					 */
					struct {
						Data::Compressor1Aux position = Data::Compressor1Aux::Off;
					} s2;
					
					/**
					 * P-D-L-S3 => Compressor 2
					 * P-D-L-S3 => Kompresor 2
					 * Circuit diagram part ... S-117 / S-118
					 */
					struct {
						Data::Compressor2 position = Data::Compressor2::Off;
					} s3;
					
					/**
					 * P-D-L-S4 => Fans
					 * P-D-L-S4 => Ventilátory
					 * Circuit diagram part ... S-113 / S-114
					 */
					struct {
						Data::Fans position = Data::Fans::Off;
					} s4;
					
					/**
					 * P-D-L-D1 => Reflector
					 * P-D-L-D1 => Dálkový světlomet
					 * Circuit diagram part ... S-141 / S-142
					 */
					struct {
						Data::Reflector position = Data::Reflector::Off;
					} d1;
					
					/**
					 * P-D-L-D2 => Cabin light
					 * P-D-L-D2 => Osvětlení kabiny
					 * Circuit diagram part ... S-151 / S-152
					 */
					struct {
						Data::Light position = Data::Light::Off;
					} d2;
					
					/**
					 * P-D-L-D3 => Instruments backlight
					 * P-D-L-D3 => Osvětlení přístrojů
					 * Circuit diagram part ... S-139 / S-140
					 */
					struct {
						Data::Light position = Data::Light::Off;
					} d3;
					
					/**
					 * P-D-L-D4 => Position (signal) lights - front right
					 * P-D-L-D4 => Návěstní (poziční) světla - přední pravá
					 * Circuit diagram part ... S-143 / S-144
					 */
					struct {
						Data::PosLight position = Data::PosLight::Off;
					} d4;
					
					/**
					 * P-D-L-D5 => Position (signal) lights - front left
					 * P-D-L-D5 => Návěstní (poziční) světla - přední levá
					 * Circuit diagram part ... S-145 / S-146
					 */
					struct {
						Data::PosLight position = Data::PosLight::Off;
					} d5;
					
					/**
					 * P-D-L-D6 => Position (signal) lights - rear right
					 * P-D-L-D6 => Návěstní (poziční) světla - zadní pravá
					 * Circuit diagram part ... S-147 / S-148
					 */
					struct {
						Data::PosLight position = Data::PosLight::Off;
					} d6;
					
					/**
					 * P-D-L-D7 => Position (signal) lights - rear left
					 * P-D-L-D7 => Návěstní (poziční) světla - zadní levá
					 * Circuit diagram part ... S-149 / S-150
					 */
					struct {
						Data::PosLight position = Data::PosLight::Off;
					} d7;
				} l;
				
				/** P-D-S */
				struct {
					/**
					 * P-D-S-KS => Main Controller - Direction cylinder
					 * P-D-S-KS => Řídicí kontrolér - Směrový válec
					 * Circuit diagram part ... S-103.A / S-104.A
					 */
					struct {
						Data::MCCDirection position = Data::MCCDirection::Neutral;
					} ks;
					
					/**
					 * P-D-S-KJ => Main Controller - Driving cylinder
					 * P-D-S-KJ => Řídicí kontrolér - Jízdní válec
					 * Circuit diagram part ... S-103.B / S-104.B
					 */
					struct {
						Data::MCCDriving position = Data::MCCDriving::Neutral;
					} kj;
					
					/**
					 * P-D-S-KB => Main Controller - Braking cylinder
					 * P-D-S-KB => Řídicí kontrolér - Brzdový válec
					 * Circuit diagram part ... S-103.C / S-104.C
					 */
					struct {
						Data::MCCBraking position = Data::MCCBraking::Neutral;
					} kb;
					
					/**
					 * P-D-S-BL => Dead-man's vigilance push-button - left
					 * P-D-S-BL => Tlačítko bdělosti - levé
					 * Circuit diagram part ... S-155 / S-156
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} bl;
					
					/**
					 * P-D-S-BP => Dead-man's vigilance push-button - right
					 * P-D-S-BP => Tlačítko bdělosti - pravé
					 * Circuit diagram part ... S-153 / S-154
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} bp;
				} s;
				
				/** P-D-P */
				struct {
					/**
					 * P-D-P-H1 => Control mode
					 * P-D-P-H1 => Režim řízení
					 * Circuit diagram part ... S-111 / S-112
					 */
					struct {
						Data::ControlMode position = Data::ControlMode::Manual;
					} h1;
					
					/**
					 * P-D-P-H2 => Driving mode (in automatic mode)
					 * P-D-P-H2 => Režim jízdy (v automatickém režimu)
					 * Circuit diagram part ... S-159 / S-160
					 */
					struct {
						Data::DrivingMode position = Data::DrivingMode::Idle;
					} h2;
					
					/**
					 * P-D-P-H3 => Limiting of relative traction
					 * P-D-P-H3 => Omezení poměrného tahu
					 * Circuit diagram part ... S-163 / S-164
					 */
					struct {
						Data::TractionLimit position = Data::TractionLimit::L12;
					} h3;
					
					/**
					 * P-D-P-H4 => Frequency of train protection system
					 * P-D-P-H4 => Přepínač kmitočtu vlakového zabezpečovače
					 * Circuit diagram part ... S-157 / S-158
					 */
					struct {
						Data::TPSFrequency position = Data::TPSFrequency::F50Hz;
					} h4;
					
					/**
					 * P-D-P-K => Speed-presets keyboard
					 * P-D-P-K => Klávesnice předvolby rychlosti
					 * Circuit diagram part ... A-114 / A-115
					 */
					struct {
						Data::SpeedKeys_t keys = 0;
					} k;
					
					/**
					 * P-D-P-D1 => Whistle
					 * P-D-P-D1 => Píšťala
					 * Circuit diagram part ... S-129 / S-130
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} d1;
					
					/**
					 * P-D-P-D2 => Slow drive (end of section, begin of train length counting)
					 * P-D-P-D2 => Pomalá jízda (konec úseku, začátek odměření délky vlaku)
					 * Circuit diagram part ... S-161 / S-162
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} d2;
					
					/**
					 * P-D-P-D3 => Train Type
					 * P-D-P-D3 => Volba druhu vlaku
					 * Circuit diagram part ... S-193 / S-194
					 */
					struct {
						Data::TrainType position = Data::TrainType::Passenger;
					} d3;
					
					/**
					 * P-D-P-D4 => Sanding
					 * P-D-P-D4 => Pískování
					 * Circuit diagram part ... S-137 / S-138
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} d4;
				} p;
				
				/** P-D-B */
				struct {
					/**
					 * P-D-B-P1 => Couplings control
					 * P-D-B-P1 => Ovládání spřáhel
					 * Circuit diagram part ... S-165 / S-166
					 */
					struct {
						Data::Coupling position = Data::Coupling::None;
					} p1;
					
					/**
					 * P-D-B-P2 => Increase of preset traction / speed
					 * P-D-B-P2 => Zvýšení předvolby tahu / rychlosti
					 * Circuit diagram part ... S-107 / S-108
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} p2;
					
					/**
					 * P-D-B-P3 => Decrease of preset traction / speed
					 * P-D-B-P3 => Snížení předvolby tahu / rychlosti
					 * Circuit diagram part ... S-105 / S-106
					 */
					struct {
						Data::PushButton pressed = Data::PushButton::Released;
					} p3;
				} b;
			} d;
		} p;
	} panels;
}; // struct Up

} // namespace MCData


#endif
