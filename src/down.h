/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData
 * DownStream data
 */
#ifndef MCDATA_DOWN_H_
#define MCDATA_DOWN_H_


#include <cstdint>

#include "mc-data/data/on_off.h"
#include "mc-data/data/power_system.h"
#include "mc-data/data/train_heating.h"


namespace MCData {

struct Down {
	/** Panels */
	struct {
		/** P */
		struct {
			/** P-H */
			struct {
				/** P-H-L */
				struct {
					/**
					 * P-H-L-BA => Battery Ammeter
					 * P-H-L-BA => Ampérmetr baterie
					 * Circuit diagram part ... P-127 / P-128
					 */
					struct {
						float value = 0;
					} ba;
					
					/**
					 * P-H-L-BV => Battery Voltmeter
					 * P-H-L-BV => Voltmetr baterie
					 * Circuit diagram part ... P-125 / P-126
					 */
					struct {
						float value = 0;
					} bv;
					
					/**
					 * P-H-L-U1 => Train Heating Indicator
					 * P-H-L-U1 => Ukazatel topení vlaku
					 * Circuit diagram part ... H-207 / H-208
					 */
					struct {
						Data::TrainHeating value = Data::TrainHeating::Off;
					} u1;
					
					/**
					 * P-H-L-U2 => Indicator of DC Main Switch
					 * P-H-L-U2 => Ukazatel DC HV
					 * Circuit diagram part ... H-203 / H-204
					 */
					struct {
						Data::OnOff value = Data::OnOff::Off;
					} u2;
					
					/**
					 * P-H-L-U3 => Indicator of troley AC/DC power system
					 * P-H-L-U3 => Ukazatel AC/DC systému v troleji
					 * Circuit diagram part ... H-201 / H-202
					 */
					struct {
						Data::PowerSystem value = Data::PowerSystem::None;
					} u3;
					
					/**
					 * P-H-L-U4 => Indicator of AC Main Switch
					 * P-H-L-U4 => Ukazatel AC HV
					 * Circuit diagram part ... H-205 / H-206
					 */
					struct {
						Data::OnOff value = Data::OnOff::Off;
					} u4;
					
					/**
					 * P-H-L-M1 => Trolley DC voltmeter
					 * P-H-L-M1 => Voltmetr DC napětí v troleji
					 * Circuit diagram part ... P-01 / P-02
					 */
					struct {
						float value = 0;
					} m1;
					
					/**
					 * P-H-L-M2 => Trolley AC voltmeter
					 * P-H-L-M2 => Voltmetr AC napětí v troleji
					 * Circuit diagram part ... P-107 / P-108
					 */
					struct {
						float value = 0;
					} m2;
					
					/**
					 * P-H-L-M3 => Traction current of motors 1-2 (1st group)
					 * P-H-L-M3 => Trakční proud motorů 1-2 (1. motorová skupina)
					 * Circuit diagram part ... P-101 / P-102
					 */
					struct {
						float value = 0;
					} m3;
					
					/**
					 * P-H-L-M4 => Exciting current
					 * P-H-L-M4 => Buzení (proud v budicích obvodech)
					 * Circuit diagram part ... P-109 / P-110
					 */
					struct {
						float value = 0;
					} m4;
					
					/**
					 * P-H-L-M5 => Traction current of motors 4-3 (2nd group)
					 * P-H-L-M5 => Trakční proud motorů 4-3 (2. motorová skupina)
					 * Circuit diagram part ... P-103 / P-104
					 */
					struct {
						float value = 0;
					} m5;
				} l;
				
				/** P-H-S */
				struct {
				} s;
				
				/** P-H-P */
				struct {
					/**
					 * P-H-P-T1 => Pressure of air reservoir and pipes
					 * P-H-P-T1 => Tlak vzduchojemu a potrubí
					 */
					struct {
						float pressure_reservoir = 0;
						float pressure_pipes = 0;
					} t1;
					
					/**
					 * P-H-P-T2 => Pressure of braking cylinders and converter
					 * P-H-P-T2 => Tlak brzdových válců a převodníku
					 */
					struct {
						float pressure_cylinders = 0;
						float pressure_converter = 0;
					} t2;
					
					/**
					 * P-H-P-P => Relative traction indicator
					 * P-H-P-P => Ukazatel poměrného tahu
					 * Circuit diagram part ... P-113 / P-114
					 */
					struct {
						float value = 0;
					} p;
					
					/**
					 * P-H-P-S1 => Indication of closed brake DAKO BSE
					 * P-H-P-S1 => Signálka závěru brzdiče DAKO BSE
					 * Circuit diagram part ... H-103 / H-104
					 */
					struct {
						bool indication = false;
					} s1;
					
					/**
					 * P-H-P-S2 => Indication of excessive flow rate of brake DAKO BSE
					 * P-H-P-S2 => Signálka průtoku brzdiče DAKO BSE
					 * Circuit diagram part ... H-105 / H-106
					 */
					struct {
						bool indication = false;
					} s2;
				} p;
				
				/** P-H-Z */
				struct {
				} z;
			} h;
			
			/** P-D */
			struct {
				/** P-D-S */
				struct {
					/**
					 * P-D-S-KS => Main Controller - Electromagnetic latch of direction cylinder
					 * P-D-S-KS => Řídicí kontrolér - Elektromagnetická západka směrového válce
					 * Circuit diagram part ... S-103.D / S-104.D
					 */
					struct {
						/** Latch engaged */
						bool latch = false;
					} ks;
				} s;
				
				/** P-D-P */
				struct {
					/**
					 * P-D-P-D2 => Slow drive (end of section, begin of train length counting) - indicator light
					 * P-D-P-D2 => Pomalá jízda (konec úseku, začátek odměření délky vlaku) - kontrolka
					 * Circuit diagram part ... S-161 / S-162
					 */
					struct {
						bool indication = false;
					} d2;
				} p;
			} d;
		} p;
	} panels;
	
	/** Panel parts backlight intensities */
	struct {
		::std::uint8_t label = 0;
		::std::uint8_t unit = 0;
	} backlight;
	
	/** Speedmeter value */
	float speed = 0;
};

} // namespace MCData


#endif
