/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Train Heating
 * Topení vlaku
 */
#ifndef MCDATA_DATA_TRAINHEATING_H_
#define MCDATA_DATA_TRAINHEATING_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME TrainHeating
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * 1500V = Train Heating connected to 1,5kV
	 * 1500V = Vlakové topení připojené na 1,5kV
	 */\
	EXT_ENUM_DEF(On1500, = -1) \
	\
	/**
	 * 0 = Heating Off
	 * 0 = Topení vypnuto
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * 3000V = Train Heating connected to 3kV
	 * 3000V = Vlakové topení připojené na 3kV
	 */\
	EXT_ENUM_DEF(On3000, = 1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
