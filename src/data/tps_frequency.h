/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Frequency of train protection system
 * Kmitočet vlakového zabezpečovače
 */
#ifndef MCDATA_DATA_TPSFREQUENCY_H_
#define MCDATA_DATA_TPSFREQUENCY_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME TPSFrequency
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * 50Hz
	 */\
	EXT_ENUM_DEF(F50Hz, = 50) \
	\
	/**
	 * 75Hz
	 */\
	EXT_ENUM_DEF(F75Hz, = 75)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
