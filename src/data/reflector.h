/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Reflector
 * Dálkový světlomet
 */
#ifndef MCDATA_DATA_REFLECTOR_H_
#define MCDATA_DATA_REFLECTOR_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Reflector
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * N2 = Reflector on with 1st tube bypassed (full power of 2nd tube)
	 * N2 = Zapnutý reflektor s přemostěním 1. výbojky (plný výkon 2. výbojky)
	 */\
	EXT_ENUM_DEF(OnBy2, = -2) \
	\
	/**
	 * N1 = Reflector on with 2nd tube bypassed (full power of 1st tube)
	 * N1 = Zapnutý reflektor s přemostěním 2. výbojky (plný výkon 1. výbojky)
	 */\
	EXT_ENUM_DEF(OnBy1, = -1) \
	\
	/**
	 * 0 = Reflector off
	 * 0 = Dálkový reflektor vypnut
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * TL = Dimmed reflector (partial power)
	 * TL = Tlumené světlo reflektoru
	 */\
	EXT_ENUM_DEF(Dimmed, = 1) \
	\
	/**
	 * P = Full reflector power
	 * P = Plné světlo reflektoru
	 */\
	EXT_ENUM_DEF(Full, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
