/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Power System
 * Napájecí systém
 */
#ifndef MCDATA_DATA_POWERSYSTEM_H_
#define MCDATA_DATA_POWERSYSTEM_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME PowerSystem
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * = = DC System
	 * = = DC systém
	 */\
	EXT_ENUM_DEF(DC, = -1) \
	\
	/**
	 * X = No power
	 * X = Bez proudu
	 */\
	EXT_ENUM_DEF(None, = 0) \
	\
	/**
	 * ~ = AC System
	 * ~ = AC systém
	 */\
	EXT_ENUM_DEF(AC, = 1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
