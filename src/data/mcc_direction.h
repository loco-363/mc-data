/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Main Controller - Direction cylinder
 * Řídicí kontrolér - Směrový válec
 */
#ifndef MCDATA_DATA_MCCDIRECTION_H_
#define MCDATA_DATA_MCCDIRECTION_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME MCCDirection
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * P = Forwards
	 * P = Vpřed
	 */\
	EXT_ENUM_DEF(Forward, = 1) \
	\
	/**
	 * 0 = Neutral
	 * 0 = Neutrál
	 */\
	EXT_ENUM_DEF(Neutral, = 0) \
	\
	/**
	 * Z = Backwards
	 * Z = Zpět
	 */\
	EXT_ENUM_DEF(Reverse, = -1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
