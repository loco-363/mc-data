/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Train Type
 * Volba druhu vlaku
 */
#ifndef MCDATA_DATA_TRAINTYPE_H_
#define MCDATA_DATA_TRAINTYPE_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME TrainType
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * OS = Passenger
	 * OS = Osobní
	 */\
	EXT_ENUM_DEF(Passenger, = 0) \
	\
	/**
	 * N = Freight
	 * N = Nákladní
	 */\
	EXT_ENUM_DEF(Freight, = 1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
