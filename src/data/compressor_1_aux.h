/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Compressor 1, Auxiliary compressor
 * Kompresor 1, Pomocný kompresor
 */
#ifndef MCDATA_DATA_COMPRESSOR1AUX_H_
#define MCDATA_DATA_COMPRESSOR1AUX_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Compressor1Aux
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * PK = Auxiliary compressor
	 * PK = Pomocný kompresor
	 */\
	EXT_ENUM_DEF(Aux, = -1) \
	\
	/**
	 * 0 = Compressors off
	 * 0 = Kompresory vypnuty
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * A = Automatic control of compressor 1
	 * A = Automatické spínání kompresoru 1
	 */\
	EXT_ENUM_DEF(Auto, = 1) \
	\
	/**
	 * R = Compressor 1 on
	 * R = Kompresor 1 zapnutý
	 */\
	EXT_ENUM_DEF(Manual, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
