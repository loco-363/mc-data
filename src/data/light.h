/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Light (with intensity)
 * Osvětlení (se změnou intenzity)
 */
#ifndef MCDATA_DATA_LIGHT_H_
#define MCDATA_DATA_LIGHT_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Light
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * 0 = Light off
	 * 0 = Osvětlení vypnuto
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * TL = Dimmed light
	 * TL = Tlumené světlo
	 */\
	EXT_ENUM_DEF(Dimmed, = 1) \
	\
	/**
	 * PL = Full light power
	 * PL = Plné světlo
	 */\
	EXT_ENUM_DEF(Full, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
