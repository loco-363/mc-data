/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Speed-presets keyboard keys
 * Tkačítka předvolby rychlosti
 */
#ifndef MCDATA_DATA_SPEEDKEYS_H_
#define MCDATA_DATA_SPEEDKEYS_H_


#include <cstdint>


namespace MCData { namespace Data {

/**
 * Type alias to allow using keys as bit flags
 */
typedef ::std::uint_least16_t SpeedKeys_t;

enum SpeedKeys : SpeedKeys_t {
	/**
	 * Preset speeds
	 * Předvolené rychlosti
	 */
	S0 = (1 << 0),
	S10 = (1 << 1),
	S20 = (1 << 2),
	S30 = (1 << 3),
	S40 = (1 << 4),
	S50 = (1 << 5),
	S60 = (1 << 6),
	S70 = (1 << 7),
	S80 = (1 << 8),
	S90 = (1 << 9),
	S100 = (1 << 10),
	S110 = (1 << 11),
	S120 = (1 << 12),
	
	/**
	 * Decrease preset speed
	 * Snížení předvolené rychlosti
	 */
	Decrease = (1 << 13),
	
	/**
	 * Increase preset speed
	 * Zvýšení předvolené rychlosti
	 */
	Increase = (1 << 14)
}; // enum SpeedKeys

}} // namespace MCData::Data


#endif
