/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Main Controller - Driving cylinder
 * Řídicí kontrolér - Jízdní válec
 */
#ifndef MCDATA_DATA_MCCDRIVING_H_
#define MCDATA_DATA_MCCDRIVING_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME MCCDriving
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * >> = NR = Incrementing fast
	 * >> = NR = Narůstání rychlé
	 */\
	EXT_ENUM_DEF(UpFast, = 2) \
	\
	/**
	 * + = NP = Incrementing slow
	 * + = NP = Narůstání pomalé
	 */\
	EXT_ENUM_DEF(UpSlow, = 1) \
	\
	/**
	 * X = DN = Neutral position (no change)
	 * X = DN = Nulová poloha (beze změny)
	 */\
	EXT_ENUM_DEF(Neutral, = 0) \
	\
	/**
	 * - = DP = Decrementing slow
	 * - = DP = Sjíždění pomalé
	 */\
	EXT_ENUM_DEF(DownSlow, = -1) \
	\
	/**
	 * << = DR = Decrementing fast
	 * << = DR = Sjíždění rychlé
	 */\
	EXT_ENUM_DEF(DownFast, = -2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
