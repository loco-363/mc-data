/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Limiting of relative traction
 * Omezení poměrného tahu
 */
#ifndef MCDATA_DATA_TRACTIONLIMIT_H_
#define MCDATA_DATA_TRACTIONLIMIT_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME TractionLimit
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * Limits
	 * Limity
	 * 1 = EDB Off
	 * 1 = EDB vyřazena
	 * 2-12 = Traction limiting
	 * 2-12 = Omezení tahu
	 * 7-12 = 100% EDB
	 */\
	EXT_ENUM_DEF(L1, = 1) \
	EXT_ENUM_DEF(L2,) \
	EXT_ENUM_DEF(L3,) \
	EXT_ENUM_DEF(L4,) \
	EXT_ENUM_DEF(L5,) \
	EXT_ENUM_DEF(L6,) \
	EXT_ENUM_DEF(L7,) \
	EXT_ENUM_DEF(L8,) \
	EXT_ENUM_DEF(L9,) \
	EXT_ENUM_DEF(L10,) \
	EXT_ENUM_DEF(L11,) \
	EXT_ENUM_DEF(L12,)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
