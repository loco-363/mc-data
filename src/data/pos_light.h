/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Position (signal) light
 * Návěstní (poziční) světlo
 */
#ifndef MCDATA_DATA_POSLIGHT_H_
#define MCDATA_DATA_POSLIGHT_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME PosLight
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * Č = Red light
	 * Č = Červené světlo
	 */\
	EXT_ENUM_DEF(Red, = -1) \
	\
	/**
	 * 0 = Light off
	 * 0 = Světlo vypnuto
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * B = White light
	 * B = Bílé světlo
	 */\
	EXT_ENUM_DEF(White, = 1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
