/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Fans
 * Ventilátory
 */
#ifndef MCDATA_DATA_FANS_H_
#define MCDATA_DATA_FANS_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Fans
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * VYP. = Fans off
	 * VYP. = Ventilátory vypnuty
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * A = Automatic control of fans
	 * A = Automatické spínání ventilátorů
	 */\
	EXT_ENUM_DEF(Auto, = 1) \
	\
	/**
	 * R = Fans on
	 * R = Ventilátory zapnuté
	 */\
	EXT_ENUM_DEF(Manual, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
