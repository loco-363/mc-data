/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Pantographs
 * Sběrače
 */
#ifndef MCDATA_DATA_PANTOGRAPHS_H_
#define MCDATA_DATA_PANTOGRAPHS_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Pantographs
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_DATA \
	/**
	 * 0 = Pantographs down
	 * 0 = Sběrače staženy
	 */\
	EXT_ENUM_DEF(None, = 0b00) \
	\
	/**
	 * P = Front pantograph (according to active cabin)
	 * P = Přední sběrač (dle stanoviště)
	 */\
	EXT_ENUM_DEF(Front, = 0b01) \
	\
	/**
	 * Z = Rear pantograph (according to active cabin)
	 * Z = Zadní sběrač (dle stanoviště)
	 */\
	EXT_ENUM_DEF(Rear, = 0b10) \
	\
	/**
	 * P+Z = Both pantographs up
	 * P+Z = Oba sběrače
	 */\
	EXT_ENUM_DEF(Both, = 0b11)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
