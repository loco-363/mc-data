/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * State of push-button
 */
#ifndef MCDATA_DATA_PUSHBUTTON_H_
#define MCDATA_DATA_PUSHBUTTON_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME PushButton
#define EXT_ENUM_TYPE bool
#define EXT_ENUM_DATA \
	EXT_ENUM_DEF(Pressed, = true) \
	EXT_ENUM_DEF(Released, = false)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
