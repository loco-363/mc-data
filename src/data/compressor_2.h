/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Compressor 2
 * Kompresor 2
 */
#ifndef MCDATA_DATA_COMPRESSOR2_H_
#define MCDATA_DATA_COMPRESSOR2_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Compressor2
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * 0 = Compressor 2 off
	 * 0 = Kompresor 2 vypnut
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * A = Automatic control of compressor 2
	 * A = Automatické spínání kompresoru 2
	 */\
	EXT_ENUM_DEF(Auto, = 1) \
	\
	/**
	 * R = Compressor 2 on
	 * R = Kompresor 2 zapnutý
	 */\
	EXT_ENUM_DEF(Manual, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
