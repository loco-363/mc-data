/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * On/Off state (switch position)
 */
#ifndef MCDATA_DATA_ONOFF_H_
#define MCDATA_DATA_ONOFF_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME OnOff
#define EXT_ENUM_TYPE bool
#define EXT_ENUM_DATA \
	EXT_ENUM_DEF(Off, = false) \
	EXT_ENUM_DEF(On, = true)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
