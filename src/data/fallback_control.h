/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Fall-back Control
 * Nouzová jízda
 */
#ifndef MCDATA_DATA_FALLBACKCONTROL_H_
#define MCDATA_DATA_FALLBACKCONTROL_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME FallbackControl
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * 0 (1st) = Disabled
	 * 0 (1.) = Vypnuto
	 */\
	EXT_ENUM_DEF(Disabled, = -1) \
	\
	/**
	 * 0 (2nd) = Fall-back Control enabled (bypass CCU = Central Control Unit)
	 * 0 (2.) = Režim nouzové jízdy (vyřazení CŘČ = centrálního řídícího členu)
	 */\
	EXT_ENUM_DEF(S0, = 0) \
	\
	/**
	 * 1-10 = Direct input of relative traction (in 10 levels)
	 * 1-10 = Přímé zadání poměrného tahu (v 10 stupních)
	 */\
	EXT_ENUM_DEF(S1,) \
	EXT_ENUM_DEF(S2,) \
	EXT_ENUM_DEF(S3,) \
	EXT_ENUM_DEF(S4,) \
	EXT_ENUM_DEF(S5,) \
	EXT_ENUM_DEF(S6,) \
	EXT_ENUM_DEF(S7,) \
	EXT_ENUM_DEF(S8,) \
	EXT_ENUM_DEF(S9,) \
	EXT_ENUM_DEF(S10,)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
