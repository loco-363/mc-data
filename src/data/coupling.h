/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Couplings
 * Spřáhla
 */
#ifndef MCDATA_DATA_COUPLING_H_
#define MCDATA_DATA_COUPLING_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME Coupling
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * 0 = Couplings inactive
	 * 0 = Spřáhla neaktivní
	 */\
	EXT_ENUM_DEF(None, = 0) \
	\
	/**
	 * P = Front coupling (according to active cabin)
	 * P = Přední spřáhlo (dle stanoviště)
	 */\
	EXT_ENUM_DEF(Front, = 1) \
	\
	/**
	 * Z = Rear coupling (according to active cabin)
	 * Z = Zadní spřáhlo (dle stanoviště)
	 */\
	EXT_ENUM_DEF(Rear, = -1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
