/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Control mode
 * Režim řízení
 */
#ifndef MCDATA_DATA_CONTROLMODE_H_
#define MCDATA_DATA_CONTROLMODE_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME ControlMode
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * R = Manual mode (SC off, direct input of relative traction to CCU)
	 * R = Ruční řízení (RR vypnutý, přímé zadání poměrného tahu do CŘČ)
	 */\
	EXT_ENUM_DEF(Manual, = 0) \
	\
	/**
	 * A = Automatic mode (SC on, input of desired speed)
	 * A = Automatické řízení (RR zapnutý, zadávání požadované rychlosti)
	 */\
	EXT_ENUM_DEF(Auto, = 1) \
	\
	/**
	 * ZK = Test of automatic mode (SC)
	 * ZK = Zkoušení automatiky (RR)
	 */\
	EXT_ENUM_DEF(TestAuto, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
