/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Main Controller - Braking cylinder
 * Řídicí kontrolér - Brzdový válec
 */
#ifndef MCDATA_DATA_MCCBRAKING_H_
#define MCDATA_DATA_MCCBRAKING_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME MCCBraking
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * - = Decrease EDB braking force
	 * - = Snížení brzdné síly EDB
	 */\
	EXT_ENUM_DEF(Decrease, = -1) \
	\
	/**
	 * B = Neutral position (no change)
	 * B = Nulová poloha (beze změny)
	 */\
	EXT_ENUM_DEF(Neutral, = 0) \
	\
	/**
	 * + = Increase EDB braking force
	 * + = Zvýšení brzdné síly EDB
	 */\
	EXT_ENUM_DEF(Increase, = 1)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
