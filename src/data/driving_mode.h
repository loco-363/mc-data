/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Driving mode (in automatic mode)
 * Režim jízdy (v automatickém režimu)
 */
#ifndef MCDATA_DATA_DRIVINGMODE_H_
#define MCDATA_DATA_DRIVINGMODE_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME DrivingMode
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * P = Parking
	 * P = Parkování
	 */\
	EXT_ENUM_DEF(Parking, = -1) \
	\
	/**
	 * V = Braking only (drive by inertia, EDB only, positive relative traction blocked)
	 * V = Výběh (jízda setrvačností, pouze EDB, zablokován kladný poměrný tah)
	 */\
	EXT_ENUM_DEF(Idle, = 0) \
	\
	/**
	 * J = Driving (full function, positive and negative relative traction)
	 * J = Jízda (plná funkce, kladný i záporný poměrný tah)
	 */\
	EXT_ENUM_DEF(Drive, = 1) \
	\
	/**
	 * S = Enable speed regulator (allow it to use positive relative traction)
	 * S = Souhlas (počáteční povolení kladného poměrného tahu)
	 */\
	EXT_ENUM_DEF(RegEnable, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
