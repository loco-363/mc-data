/**
***********************************************
***          Loco363 - ModuCab Data         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * MCData::Data
 * Power System Switching & Main Switch
 * Přepínání systémů + hlavní vypínač
 */
#ifndef MCDATA_DATA_POWERSYSTEMSWITCH_H_
#define MCDATA_DATA_POWERSYSTEMSWITCH_H_


namespace MCData { namespace Data {

#define EXT_ENUM_NAME PowerSystemSwitch
#define EXT_ENUM_TYPE char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * ZAP. = Turn on DC Main Switch, Start DC System
	 * ZAP. = Zapnutí DC HV, start DC systému
	 */\
	EXT_ENUM_DEF(DCStart, = -2) \
	\
	/**
	 * = = DC System choosed
	 * = = Volba DC systému
	 */\
	EXT_ENUM_DEF(DCOn, = -1) \
	\
	/**
	 * 0 = Main Switches Off
	 * 0 = Vypnutí hlavních vypínačů
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * ~ = AC System choosed
	 * ~ = Volba AC systému
	 */\
	EXT_ENUM_DEF(ACOn, = 1) \
	\
	/**
	 * ZAP. = Turn on AC Main Switch, Start AC System
	 * ZAP. = Zapnutí AC HV, start AC systému
	 */\
	EXT_ENUM_DEF(ACStart, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "mc-data/lib/ext_enum/src/ext_enum.h"

}} // namespace MCData::Data


#endif
